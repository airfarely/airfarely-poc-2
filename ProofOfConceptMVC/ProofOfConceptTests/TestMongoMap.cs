﻿using System;
using System.Collections.Generic;
using System.Text;
using ProofOfConceptAPI.Models;
using Microsoft.VisualStudio.TestTools;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Bson;

namespace ProofOfConceptTests
{
    [TestClass]
    public class TestMongoMap
    {
        private TestContext m_testContext;

        public TestContext TestContext
            {
                get { return m_testContext; }
                set { m_testContext = value; }
            }

        [TestMethod]
        public void TestBsonClassMap()
        {
            
                    AssemblyInitialize(TestContext);
                
            //arrange
             MongoClient mongo = new MongoClient("mongodb://localhost:27017");
            var db = mongo.GetDatabase("AirFareLy");
            var collection = db.GetCollection<BsonDocument>("ProofOfConceptDb");

            Dictionary<String, Dictionary<String, DateTime>> windowDict = new Dictionary<string, Dictionary<string, DateTime>>();
            Dictionary<String, DateTime> bounds = new Dictionary<string, DateTime>();
            
            int[] departures = new int[24];
            for (int i = 0; i < 24; i++) {
                departures[i] = i;
                }
            
            int[] arrivals = new int[24];
             for (int i = 0; i < 24; i++) {
                arrivals[i] = i;
                }
            
            Dictionary<String, Dictionary<String, int>> airlinePrefs = new Dictionary<string, Dictionary<string, int>>();
            
            for (int i = 0; i < 5; i++) 
                {
                    Dictionary<String, int> testDict = new Dictionary<string, int>();
                for (int k = 0; k < 3; k++) 
                    { 
                        var key2 = k.ToString();
                        testDict.Add(key2, k);
                    }
                if (!airlinePrefs.ContainsKey(i.ToString())) 
                    {
                        airlinePrefs.Add(i.ToString(), testDict);
                    }
                }
                    //testDict.TryGetValue(j.ToString(), out currDict);
                    
                
            int[] layoverPrefs = new int[5] { 11, 22, 44, 66, 78};

            //for id, how to get automatically generated guid?
            //or just a regularly generated unique random id
            //temp-hard-coded
            PreferencesItem prefs = new PreferencesItem(8, windowDict, bounds, departures, arrivals, airlinePrefs, layoverPrefs);
            //act
            var prefDoc = prefs.ToBsonDocument<PreferencesItem>();
            //to check for id (_id is seemingly the public accessor, but I 
            //don't see where that is generated, or how that is labeled)
            collection.InsertOne(prefDoc);

            var document = new BsonDocument
            {
                { "name", "MongoDB" },
                { "type", "Database" },
                { "count", 1 },
                { "info", new BsonDocument
                    {
                        { "x", 203 },
                        { "y", 102 }
                    }}
            };
            var readOut = document.ToString();
            collection.InsertOne(document);
            //assert
            //the string below MAY be Json...
            var doc = BsonDocument.Parse("{\"name\": \"shmoops\", \"type\": \"dataOops\", \"info\": [{\"x\": 247, \"y\":183}]}");
            //uh oh, looks like it will just keep inserting this doc...
            //gotta do a contains check
            
            collection.InsertOne(doc);

            //Do not do this, LOL...unless resetting the db to NONEXISTENT, haha!
            //mongo.DropDatabase("AirFareLy");
            
        }

        [AssemblyInitialize()]
public static void AssemblyInitialize(TestContext tc) {


    //TestContext = tc;
    tc.WriteLine("In AssemblyInitialize");
    if (!BsonClassMap.IsClassMapRegistered(typeof(PreferencesItem)))
    {
        BsonClassMap.RegisterClassMap<PreferencesItem>();
    }
}
        [AssemblyCleanup()]
        public static void AssemblyCleanup() {
          // TODO: Clean up after all tests in an assembly
          
        }
    }
}
