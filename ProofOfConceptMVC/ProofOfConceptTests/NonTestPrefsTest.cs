﻿using System;
using System.Collections.Generic;
using System.Text;
using ProofOfConceptAPI.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using ProofOfConceptAPI.Controllers;

namespace ProofOfConceptTests
{
    class NonTestPrefsTest
    {
        public void SimplestTest()
        {
             PreferencesItem prefs = new PreferencesItem(47);
        }

        public void TheNextSteps()
        {
            PreferencesItem prefs = new PreferencesItem(47);
            var optionsBuilder = new DbContextOptionsBuilder<PreferencesContext>();
            optionsBuilder.UseSqlite("Data Source=Preferences.db");
            using (var context = new PreferencesContext(optionsBuilder.Options))
            {
                var count = context.PreferencesItems.Count();
            

            }
        }

        public void TheFinalSteps()
        {
             DbContextOptions<PreferencesContext> dbContextOptions = new DbContextOptions<PreferencesContext>();
            PreferencesContext context = new PreferencesContext(dbContextOptions);

            PreferencesController controller = new PreferencesController(context);
            var count = context.PreferencesItems.CountAsync();
        }
    }
}
