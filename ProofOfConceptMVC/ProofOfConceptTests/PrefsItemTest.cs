using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProofOfConceptAPI.Models;
using ProofOfConceptAPI.Controllers;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace ProofOfConceptTests
{
    [TestClass]
    public class PrefsItemTest
    {
        [TestMethod]
        public void ConstructorTest()
        {
            PreferencesItem prefs = new PreferencesItem(47);

            //act

            //assert passes
            Assert.IsNotNull(prefs);
        }

         [TestMethod]
        public void ContextToController()
        {
            //no db set up to work with this dbcontext 
            PreferencesItem prefs = new PreferencesItem(47);
            var optionsBuilder = new DbContextOptionsBuilder<PreferencesContext>();
            optionsBuilder.UseSqlite("Data Source=Preferences.db");
            using (var context = new PreferencesContext(optionsBuilder.Options))
            {
                var count = context.PreferencesItems.Count();
            Assert.AreEqual(count, 1);

            }
            //DbContextOptions<PreferencesContext> dbContextOptions = new DbContextOptions<PreferencesContext>();
            //PreferencesContext context = new PreferencesContext(dbContextOptions);

            //PreferencesController controller = new PreferencesController(context);
            
        }

         [TestMethod]
        public async Task ContextToControllerWithNull()
        {
            
            DbContextOptions<PreferencesContext> dbContextOptions = new DbContextOptions<PreferencesContext>();
            PreferencesContext context = new PreferencesContext(dbContextOptions);

            PreferencesController controller = new PreferencesController(context);
            var count =  await context.PreferencesItems.CountAsync();
            
            Assert.AreEqual(count, 1);
        }
    }
   
}
