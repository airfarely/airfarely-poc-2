﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;

namespace ProofOfConceptMVC
{
    public class Playground
    {


        private class MessingAround
        {
           void MessingAroundRightGood()
            {
                //used synchronously
                //also using a using bloack can actually induce socket exception fail
                //rather static initialization better (seen below ((with .net.http using above)))
                 using (var httpClient = new HttpClient())
                {
	                httpClient.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
 
	               // var response = httpClient.GetStringAsync(new Uri(url)).Result;
 
	               // var releases = JArray.Parse(response);
                }
            }
        }

        private class Author
        {
          
            
            public async Task<Author> GetAuthorsAsync(string uri)
            {
                //what does a handler stack do? I mean I have my informed guesses (i.e. hypos)
                ////looks like I could put  all the code below in a using block, like below:    
                //string data = null;
                //    using (var webClient = new WebClient())
                //    {
                //        data = webClient.DownloadString(url);
                //    }
                HttpClient client = new HttpClient();

                Author author = null;
                //i beleive this  uri is the place to look for a 'get' or POST
                HttpResponseMessage response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    //i believe this line depended on a extension method, or an outdated one
                    //author = await response.Content.ReadAsAsync<Author>();
                    //have choice of readasString or readasStream
                     author = await response.Content.ReadAsStringAsync();
                }
//    If you would like to throw an exception if the IsSuccessStatusCode property is false, 
//    you can make a call to the EnsureSuccessStatusCode method on the response instance as shown below.

//response.EnsureSuccessStatusCode();
            return author;
            }

            public static implicit operator Author(string v)
            {
                throw new NotImplementedException();
            }
        }
    }
}
