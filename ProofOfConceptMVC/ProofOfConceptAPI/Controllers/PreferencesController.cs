﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProofOfConceptAPI.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProofOfConceptAPI.Controllers
{
    [Route("api/[controller]")]
    public class PreferencesController : Controller
    {
        private readonly PreferencesContext _context;
        public PreferencesController(PreferencesContext context)
        {
            _context = context;

            //problem  line!!!!
            if (_context.PreferencesItems.Count() == 0)
            {
                _context.PreferencesItems.Add(new PreferencesItem(1) { });
                //should make this saveChangesAsync?
                _context.SaveChanges();
            }
        }

        // GET: api/values all
        //is this GET /api/Preferences
        //? because it drops the Controller from the class?
        //or what?
        [HttpGet]
        public IEnumerable<PreferencesItem> GetAll()
        {
            return _context.PreferencesItems.ToList();
        }

        // GET api/values/5 <-- probly  not this anymore
        //HAHA just read further, and I am confirmed in prev. interpretation

        //link to this route in an HTTP  Response:
        // GET api/preferences/{id}
        [HttpGet("{id}", Name = "GetPrefs")]
        public IActionResult GetById(long id)
        {
            var item = _context.PreferencesItems.FirstOrDefault(t => t.Id == id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
