﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MongoDB.Bson.Serialization;
using ProofOfConceptAPI.Models;

namespace ProofOfConceptAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //i believe that this registerClassMap should go in startup.
            //but didn't know how to get that to work with a test
            BsonClassMap.RegisterClassMap<PreferencesItem>();
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
