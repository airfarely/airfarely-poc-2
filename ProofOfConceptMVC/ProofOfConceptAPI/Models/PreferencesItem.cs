﻿using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProofOfConceptAPI.Models
{
    public class PreferencesItem
    {
        //may want to change this id from long to guid in the future
        private long id;

        [BsonId]
        public long Id
        {
            get { return id; }
            set { id = value; }
        }

        private Dictionary<String, Dictionary<String, DateTime>> windows; 
            private Dictionary<String, DateTime> windowFrame;
        private int[] departMap;
        private int[] arriveMap;
        private Dictionary<String, Dictionary<String, int>> airlines;
        private int[] layover;

        public PreferencesItem()
        {
        }

        public PreferencesItem(long idVal): this(idVal, new Dictionary<string, Dictionary<string, DateTime>>(), 
            new Dictionary<string, DateTime>(), new int[24], new int[24], 
            new Dictionary<string, Dictionary<string, int>>(), new int[6]) 
        {
        }

        //validate logic goes in this constructor
        public PreferencesItem(long currId, Dictionary<String,Dictionary<String, DateTime>> windowDict, 
            Dictionary<String, DateTime> bounds, int[] departures, int[] arrivals, 
            Dictionary<String, Dictionary<String, int>> airlinePrefs, int[] layoverPrefs)
        {
            
            id = currId;
            //ok...so windowDict is actually comprised of bounds, must check how to construct
            windows = windowDict;
            windowFrame = bounds;
            departMap = departures;
            arriveMap = arrivals;
            airlines = airlinePrefs;
            layover = layoverPrefs;
        }

        //[JsonConverter(typeof(Dictionary<String, Dictionary<String, DateTime>>))]
        
        public Dictionary<String, Dictionary<String, DateTime>> Windows
        {
            get { return windows; }
            set { windows = value; }
        }
        
        public Dictionary<string, DateTime> WindowFrame
        {
            get { return windowFrame; }
            set { windowFrame = value; }
        }
        
        public int[] DepartMap
        {
            get { return departMap; }
            set { departMap = value; }
        }
        
        public int[] ArriveMap
        {
            get { return arriveMap; }
            set { arriveMap = value; }
        }
        
        public Dictionary<string, Dictionary<string, int>> Airlines
        {
            get { return airlines; }
            set { airlines = value; }
        }
        
        public int[] Layover
        {
            get { return layover; }
            set { layover = value; }
        }

        //actually using Bson documents i think
        private void JSONSerialize()
        {
            PreferencesItem prefs = new PreferencesItem(12);
            string jsonData = JsonConvert.SerializeObject(prefs);
        }
        

    }
}
