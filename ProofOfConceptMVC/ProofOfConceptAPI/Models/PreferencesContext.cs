﻿using Microsoft.EntityFrameworkCore;

namespace ProofOfConceptAPI.Models
{
    public class PreferencesContext : DbContext
    {
        public PreferencesContext(DbContextOptions<PreferencesContext> options)
            : base(options)
        {
        }

        public DbSet<PreferencesItem> PreferencesItems { get; set; }

    }
}