import resources   as rc
import flightPrefs 
import pdb
import optionSet
import userPrefs 

options = optionSet.optionSet()
up = userPrefs.userPrefs()
fp = flightPrefs.flightPrefs()
tp = rc.getTripPrefs()

for day in ['SAT', 'SUN', 'MON']:

	flights = rc.getFlights(day) 

	options.addViable(day, flights, fp, tp, up)

options.printByPrice()
