import pdb
import datetime

class flightPrefs(dict): 

	def __init__(self):

	    p = {}

	    self['days'] = ['SAT', 'SUN', 'MON']
	    self['windows']   = {}
	    self['premiums']  = {}

	    #Departure Windows (contraints)

	    self['windows']['dept']  = []
	    self['windows']['arrv']  = []

	    self['windows']['dept'].append(('SAT', 17, 23))
	    self['windows']['dept'].append(('SUN', 0,  23))
	    self['windows']['dept'].append(('MON', 0,  23))
	    self['windows']['arrv'].append(('MON', 0,  21))

	    #Premium conditions

	    self['premiums']['dept']  = []
	    self['premiums']['arrv']  = []

	    self['premiums']['dept'].append(('SUN', 14, 23, 60))
	    self['premiums']['arrv'].append(('SUN', 0,  19, 140))
	    self['premiums']['arrv'].append(('SUN', 0,  21, 100))
	    self['premiums']['arrv'].append(('MON', 0,  19, 40))

	    #Misc

	    self.timeTemplate = datetime.datetime.now()

	def getWindows(self, target):
		
	    w = {}
	    w['dept']  = []
	    w['arrv']  = []

	    for day, start, end in self['windows']['dept']:
		if day == target:
			w['dept'].append((self.wrapTime(start), self.wrapTime(end)))

	    for day, start, end in self['windows']['arrv']:
		if day == target:
			w['arrv'].append((self.wrapTime(start), self.wrapTime(end)))

	    if len(w['dept']) == 0:
			w['dept'].append((self.wrapTime(0), self.wrapTime(23)))

	    if len(w['arrv']) == 0:
			w['arrv'].append((self.wrapTime(0), self.wrapTime(23)))
	
	    return w
	
	def getPremiums(self, target):

	    p = {}
	    p['dept']  = []
	    p['arrv']  = []

	    for day, start, end, premium in self['premiums']['dept']:
		if day == target:
			p['dept'].append((self.wrapTime(start), self.wrapTime(end), premium))

	    for day, start, end, premium in self['premiums']['arrv']:
		if day == target:
			p['arrv'].append((self.wrapTime(start), self.wrapTime(end), premium))

	    return p

	def wrapTime(self, h):

	    return self.timeTemplate.replace(hour=h, minute=0, second=0, microsecond=0).time()

