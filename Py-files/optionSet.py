import json
import pprint
import sys
import pdb
import math
from datetime import datetime
import re

class optionSet(list):

	def addViable(self, day, flights, flightPrefs, tripPrefs, userPrefs):

		windows  = flightPrefs.getWindows(day)
		premiums = flightPrefs.getPremiums(day)

		for flight in flights:

			inDW       = False
			inAW       = False
			affordable = False
			tolerable  = False

			option = flightOption(day, flight)

			if option['price'] <= tripPrefs['oopMax']:
				affordable = True

			if option['connections'] <= tripPrefs['maxCon']:
				tolerable = True

			for dw in windows['dept']:
				if dw[0] < option['dept'] < dw[1]:
					inDW = True

			for aw in windows['arrv']:
				if aw[0] < option['arrv'] < aw[1]:
					inAW = True

			if inDW and inAW and affordable and tolerable:
				option.calculateEffective(tripPrefs, userPrefs)
				self.append(option)


	def printCurrent(self):

		for option in self:
			print(option)

	def printByPrice(self):

		byPrice = sorted(self, key=lambda x: x['price']) 

		for option in byPrice:
			print(option)

	#def printByBest(self, up2):

	    #sorted = sort(self, lambda: x['effectivePrice'], x) 
	    #for option in self:
		#option.display()

class flightOption(dict):

	def __init__(self, day, raw):

		self['day'] = day

		self['price']        = int(math.ceil(float(raw['pricing'][0]['saleTotal'].replace('USD', ''))))

		legs              = raw['slice'][0]['segment']
		self['connections']  = len(legs) - 1

		for leg in legs:

		    if 'dept' not in self:
			    self['dept']        = self.cleanTime(leg['leg'][0]['departureTime'])
		    if 'origin' not in self:
			    self['origin']      = leg['leg'][0]['origin']

		    self['destination'] = leg['leg'][0]['destination']
		    self['arrv']        = self.cleanTime(leg['leg'][0]['arrivalTime'])

		    carrier     = leg['flight']['carrier']
		    number      = leg['flight']['number']
		    duration    = leg['duration']

	def cleanTime(self, rawTime):

		m = re.match('^(.*)[A-Z](.*)\-.*$', rawTime)

		cleaner = m.group(1)+' '+m.group(2)

		cleaned = datetime.strptime(cleaner, '%Y-%m-%d %H:%M' )

		return cleaned.time()

	def calculateEffective(self, premiums, userPrefs):
	    effective = self['price']
            print('\nMonetary Price: ' + str(self['price']))

'''
	def display(self, raw)

            print('\nMonetary Price: ' + self['mPrice'])
            print('\nEffective Price: ' + self['ePrice'])

            print('\t{} {} {} {} {} {}'.format(self['carrier'], self['number'], self['origin'], self['dTime'], self['destination'], self['aTime']))

        print('')
'''

print 'exit'

