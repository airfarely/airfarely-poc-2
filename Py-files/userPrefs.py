import pdb

def userPrefs(): 

    p = {}

    p['airline']   = {}
    p['departure'] = []
    p['arrival']   = []
    p['layover']   = []
    p['inflight']  = []

    #Airline preferences (cdr)

    p['airline']['delta']      = 0 
    p['airline']['united']     = 10 
    p['airline']['southwest']  = 20 
    p['airline']['suncountry'] = 25 
    p['airline']['jetblue']    = 25 
    p['airline']['american']   = 25 
    p['airline']['frontier']   = 35 
    p['airline']['spirit']     = 40 

    #Departure preferences (cdr)

    p['departure'].append(50)
    p['departure'].append(60)
    p['departure'].append(70)
    p['departure'].append(80)
    p['departure'].append(80)
    p['departure'].append(70)
    p['departure'].append(70)
    p['departure'].append(60)
    p['departure'].append(50)
    p['departure'].append(35)
    p['departure'].append(20)
    p['departure'].append(0)
    p['departure'].append(0)
    p['departure'].append(0)
    p['departure'].append(0)
    p['departure'].append(0)
    p['departure'].append(0)
    p['departure'].append(0)
    p['departure'].append(0)
    p['departure'].append(10)
    p['departure'].append(20)
    p['departure'].append(30)
    p['departure'].append(30)
    p['departure'].append(40)

    #Arrival preferences (cdr)

    p['arrival'].append(60)
    p['arrival'].append(60)
    p['arrival'].append(60)
    p['arrival'].append(70)
    p['arrival'].append(70)
    p['arrival'].append(70)
    p['arrival'].append(60)
    p['arrival'].append(30)
    p['arrival'].append(20)
    p['arrival'].append(10)
    p['arrival'].append(0)
    p['arrival'].append(0)
    p['arrival'].append(0)
    p['arrival'].append(0)
    p['arrival'].append(0)
    p['arrival'].append(10)
    p['arrival'].append(20)
    p['arrival'].append(30)
    p['arrival'].append(30)
    p['arrival'].append(30)
    p['arrival'].append(30)
    p['arrival'].append(35)
    p['arrival'].append(40)
    p['arrival'].append(50)

    #Layover preferences (cdr)

    p['layover'].append(0)
    p['layover'].append(40)
    p['layover'].append(45)
    p['layover'].append(50)
    p['layover'].append(55)
    p['layover'].append(60)
    p['layover'].append(65)

    return p
