import json

def getFlights(day):

	#Replace with call to API when we get there

	data    = json.loads(open('./sampleData/'+day+'.txt').read())
	flights = data['trips']['tripOption']

	return flights

def getTripPrefs():

	p = {}

	p['minStay'] = None
	p['maxStay'] = None
	#p['oopMax']  = 420
	p['oopMax']  = 750
	p['maxCon']  = 1

	return p
